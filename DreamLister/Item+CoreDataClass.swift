//
//  Item+CoreDataClass.swift
//  DreamLister
//
//  Created by Visakeswaran N on 09/08/17.
//  Copyright © 2017 Visakeswaran N. All rights reserved.
//

import Foundation
import CoreData

@objc(Item)
public class Item: NSManagedObject {

    public override func awakeFromInsert() {
        
        self.awakeFromInsert()
        
        self.date = NSDate()
        
    }
    
}
